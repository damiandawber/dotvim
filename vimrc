"
" Suggested plugins:
"
" NERDTree
" CSApprox
" snipMate
" VCScommandplugin
"
" Don't forget to add `export TERM="xterm-256color"` to bashrc
"

syntax on
syntax enable

" line numbers
set number

" Highlight searches - note that you can turn off temporarily with <C-L>
" -- See next set of config options
set hlsearch
set ignorecase
set smartcase " If capitals are used, searching is case-sensitive

" Map <C-L> to also turn off search highlighting until next search
nnoremap <C-L> :nohl<CR><C-L>

"CTags and Taglist plugin
"apt-get install exuberant-ctags
let Tlist_Ctags_Cmd = "/usr/bin/ctags"
let Tlist_WinWidth = 50
map <F3> :TlistToggle<cr>
map <F10> :!/usr/bin/ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR>
set tags+=./tags,tags;
nnoremap <C-]> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>

" Command window height of 2 lines avoids the 'enter to continue' crap
set cmdheight=2

au BufNewFile,BufRead *.ctp set filetype=php " Cake template files
au BufNewFile,BufRead *.module set filetype=php " Drupal module file
au BufNewFile,BufRead *.phtml set filetype=php " ZF templates

" Set proper match_words variable for matchit.vim
filetype plugin on

" Allow the cursor to go in to "invalid" places
set virtualedit=all

" Tab key. Uses spaces instead of the hard tab.
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab

" Cursor configuration
set guicursor=n-v-c:block-Cursor-blinkon0
set guicursor+=ve:ver35-Cursor
set guicursor+=o:hor50-Cursor
set guicursor+=i-ci:ver25-Cursor
set guicursor+=r-cr:hor20-Cursor
set guicursor+=sm:block-Cursor-blinkwait175-blinkoff150-blinkon175


" Not really too bothered about backups since file changes should be
" accounted for by SCM
set nobackup
set nowb
set noswapfile

" When the page starts to scroll, keep the cursor 8 lines from
" the top and 8 lines from the bottom
set scrolloff=8

" Scroll page as searching
set incsearch

" Syntax coloring lines that are too long just slows down the world
set synmaxcol=2048

" Map to PHP manual
nnoremap <C-p> :!firefox -new-tab http://php.net/<cword><C-m>
nnoremap <C-a> :VCSVimDiff<CR>

nnoremap <F4> :NERDTreeTabsToggle<CR>
nnoremap <F5> :TlistToggle<CR>

" Pasting
set pastetoggle=<F2>

" PHP Folding
map <F5> <Esc>:EnableFastPHPFolds<Cr>
map <F6> <Esc>:EnablePHPFolds<Cr>
map <F7> <Esc>:DisablePHPFolds<Cr>

let g:DisableAutoPHPFolding = 1

" NerdTree tabs plugin
let g:nerdtree_tabs_open_on_console_startup=1

" Whenever VISUAL mode is started, or the Visual area extended, Vim tries to become the owner of
" the windowing system's global selection.  This means that the visually highlighted text is available 
" for pasting into other applications as well as into Vim itself.
set go=a

" vim -p `some command` is useful for opening mulitple files in multiple tabs
" increase the limit on the number of tabs vim opens by default
set tabpagemax=50

" Pathogen
execute pathogen#infect()

" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_open = 1
" let g:syntastic_check_on_wq = 0
" let g:syntastic_javascript_checkers = ['jshint']

" Set colorscheme
" Note that we can get Terminal to approximate a 256 color terminal by
" by setting the PATH variable in .bashrc
" We also use CSApprox plugin to enable gvim-based themes to work
" in ther terminal
"set termguicolors
set t_Co=256
colorscheme monokai
