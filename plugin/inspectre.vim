function! s:inspectre()
    echo "Running inspectre, please wait..."
    if expand('<cword>') == expand('<cfile>')
        exec(system('inspectre-vim ' . shellescape(expand('<cword>')) . ' ' . shellescape(expand('%:p'))))
    else
        exec(system('inspectre-vim -t file ' . shellescape(expand('<cfile>')) . ' ' . shellescape(expand('%:p'))))
    endif
endfunction
command Inspectre :call <SID>inspectre()
nmap <Space> :Inspectre<CR>

function! s:inspectreCall()
    echo "Running inspectre, please wait..."
    exec(system('inspectre-vim -t call ' . shellescape(expand('<cword>')) . ' ' . shellescape(expand('%:p'))))
endfunction
command InspectreCall :call <SID>inspectreCall()
nmap c<Space> :InspectreCall<CR>
